![Caper](doc/small_caper.jpg)

# Caper
Caper is a tool for understanding pcap expressions in the following ways:
* What does the pcap expression expand to "in full".
* Whether one expressions accepts all the packets accepted by the other (and possibly more).
* Whether the two expressions are equivalent (i.e., they accept/reject the same packets).

More info can be found in [the Caper paper](https://www.seas.upenn.edu/~nsultana/files/pcap_semantics.pdf).

# Usage
Build by running `./build.sh` then run `./caper.byte` for usage info.

Alternatively, use the [Vagrantfile](Vagrantfile) that sets up a Debian 11 VM
with the dependencies needed to compile Caper, and then compiles it.

The following show examples of using Caper to fully expand pcap expressions:

```
$ ./caper.byte -q -r -e "dst port http"
(ether proto \ip && ip proto \sctp && sctp dst port 80) || (ether proto \ip6 && ip6 proto \sctp && sctp dst port 80) || (ether proto \ip && ip proto \udp && udp dst port 80) || (ether proto \ip6 && ip6 proto \udp && udp dst port 80) || (ether proto \ip && ip proto \tcp && tcp dst port 80) || (ether proto \ip6 && ip6 proto \tcp && tcp dst port 80)
```

```
$ ./caper.byte -q -r -e "src foo"
(ether proto \rarp && rarp src host foo) || (ether proto \arp && arp src host foo) || (ether proto \ip && ip src host foo)
```

# License
Caper is released under [GPLv3 or any later version](COPYING)

# Contributors
[Nik Sultana](https://www.nik.network)  
Hyunsuk Bang  
Denis Ovsienko  
Marelle Leon  
