#!/bin/bash
# Caper: a pcap expression analysis utility.
# Nik Sultana, November 2018.

DEFAULT_TARGET=caper.byte

if [ -z $1 ]
then
  TARGET=${DEFAULT_TARGET}
else
  TARGET="${1}"
fi

echo "building ${TARGET}"

# NOTE could add -dont-catch-errors to have exceptions pass through catches.
ocamlbuild -cflag -g -lflag -g -tag thread -use-ocamlfind -use-menhir \
  -no-hygiene \
  -package str \
  -I syntax \
  ${TARGET}
