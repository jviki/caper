(*
  Copyright Hyunsuk Bang, December 2022
  Copyright Nik Sultana, December 2022

  This file is part of Caper.

  Caper is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Caper is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Caper.  If not, see <https://www.gnu.org/licenses/>.


  This module: HTML marking-up of output
*)

let make_paragraph psg = "<p>" ^ psg ^ "</p>";;
let append_new_line line = line ^ " <br/>";;
let make_bold word = "<b>" ^ word ^ "</b>";;
let space_out s = " &nbsp; " ^ s ^ " &nbsp; ";;
let html_entity word =
  match word with
  | "||" -> space_out "||"
  | "&&" -> space_out "&amp;&amp;"
  | "&" -> "&amp;"
  | "<<" -> "&lt;&lt;"
  | "<" -> "&lt;"
  | "<=" -> "&lt;="
  | ">>" -> "&gt;&gt;"
  | ">" -> "&gt;"
  | ">=" -> "&gt;="
  | _ -> word

(*FIXME instead of hardcoding the formatting, reference classes/style to draw formatting from elsewhere, making the formatting parametric*)
let make_keyword_bold word =
  match word with
  | "host" | "net" | "port" | "portrange" | "proto"
  | "ether"| "vlan" | "mlps" | "ip" | "ip6" | "arp" | "rarp" | "icmp" | "tcp" | "udp" | "sctp" | "scp" | "icmp6"
  | "src" | "dst"
  | "and" | "or" | "not" -> make_bold (html_entity word)

  | "&&" | "||" | "&" | "|"
  | "+" | "-" | "*" | "/" | "%"
  | "<" | "<<" | ">" | ">>" | ">=" | "<="
  | "=" | "!=" | "==" -> html_entity word (* NOTE symbolic operators are not made bold, to reduce clutter *)

  | _ -> word

let rec backslash (word : string) =
  match String.get word 0 with
  | '\\' -> "\\" ^ (make_keyword_bold (String.sub word 1 ((String.length word) - 1)))
  | '\t' -> backslash ("\\t" ^ (String.sub word 1 ((String.length word) - 1))) (* edge case for \tcp.*)
  | _ -> make_keyword_bold word

let rec square_bracket (paren : char) word =
  word
  |> String.split_on_char paren
  |> List.map make_keyword_bold
  |> String.concat (make_bold (String.make 1 paren))

let rec paren_helper (paren : char) (counter : int) =
  if counter == 0 then ""
  else make_bold (String.make counter paren)

let rec open_paren (counter : int) (word : string) =
  match (String.get word 0) with
  | '(' -> open_paren (counter + 1) (String.sub word 1 ((String.length word) - 1))
  | _ -> (paren_helper '(' counter) ^ (square_bracket '[' word)

let rec close_paren (counter : int) (word : string) =
  match (String.get word (String.length word - 1)) with
  | ')' -> close_paren (counter + 1) (String.sub word 0 (String.length word - 1))
  | _ -> (square_bracket ']' word) ^ (paren_helper ')' counter)

let process_line line =
  line
  |> String.split_on_char ' '
  |> List.map backslash
  |> List.map (open_paren 0)
  |> List.map (close_paren 0)
  |> String.concat " "
(* FIXME consider re-enabling the following, based on expectations of downstream usage:
  |> make_paragraph
  |> append_new_line
*)
