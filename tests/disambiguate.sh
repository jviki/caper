#!/bin/bash
#
# Caper: a pcap expression analysis utility.
# Regression tests for disambiguation.
# Nik Sultana, October 2022

CAPER="$1"
TEST_SCRIPT="$2"

function test() {
  ${TEST_SCRIPT} "${CAPER}" "$1" "$2"
}

test "arp[0] = 0" "ether proto \arp && arp[0] = 0"
test "atalk[0] = 0" "atalk[0] = 0" # TODO add expected result
test "carp[0] = 0" "carp[0] = 0" # TODO add expected result
test "decnet[0] = 0" "decnet[0] = 0" # TODO add expected result
test "ether[0] = 0" "ether[0] = 0"
test "fddi[0] = 0" "fddi[0] = 0" # TODO add expected result
test "icmp[0] = 0" "ether proto \ip && ip proto \icmp && icmp[0] = 0"
test "icmp6[0] = 0" "ether proto \ip6 && ip6 proto \icmp6 && icmp6[0] = 0"
test "igmp[0] = 0" "igrp[0] = 0" # TODO add expected result
test "igrp[0] = 0" "igrp[0] = 0" # TODO add expected result
test "ip[0] = 0" "ether proto \ip && ip[0] = 0"
test "ip6[0] = 0" "ether proto \ip6 && ip6[0] = 0"
test "lat[0] = 0" "lat[0] = 0" # TODO add expected result
test "link[0] = 0" "link[0] = 0" # TODO add expected result
test "mopdl[0] = 0" "mopdl[0] = 0" # TODO add expected result
test "moprc[0] = 0" "moprc[0] = 0" # TODO add expected result
test "pim[0] = 0" "pim[0] = 0" # TODO add expected result
test "ppp[0] = 0" "ppp[0] = 0" # TODO add expected result
test "radio[0] = 0" "radio[0] = 0" # TODO add expected result
test "rarp[0] = 0" "rarp[0] = 0" # TODO add expected result
test "sca[0] = 0" "sca[0] = 0" # TODO add expected result
test "sctp[0] = 0" "sctp[0] = 0" # TODO add expected result
test "slip[0] = 0" "slip[0] = 0" # TODO add expected result
test "tcp[0] = 0" "ether proto \ip && ip proto \tcp && tcp[0] = 0"
test "tcp[0:2] = 0" "ether proto \ip && ip proto \tcp && tcp[0 : 2] = 0"
test "tcp[0:2] == 0" "ether proto \ip && ip proto \tcp && tcp[0 : 2] = 0"
test "tr[0] = 0" "tr[0] = 0" # TODO add expected result
test "udp[0] = 0" "ether proto \ip && ip proto \udp && udp[0] = 0"
test "vrrp[0] = 0" "vrrp[0] = 0" # TODO add expected result
test "wlan[0] = 0" "wlan[0] = 0" # TODO add expected result

# FIXME This should also generate expression alternatives involving IPv6.
# NOTE these tests are for -n; the default uses names.
test "tcp[tcpflags] = tcp-fin" "ether proto \ip && ip proto \tcp && tcp[13] = 0x01"
test "tcp[tcpflags] = tcp-syn" "ether proto \ip && ip proto \tcp && tcp[13] = 0x02"
test "tcp[tcpflags] = tcp-rst" "ether proto \ip && ip proto \tcp && tcp[13] = 0x04"
test "tcp[tcpflags] = tcp-push" "ether proto \ip && ip proto \tcp && tcp[13] = 0x08"
test "tcp[tcpflags] = tcp-ack" "ether proto \ip && ip proto \tcp && tcp[13] = 0x10"
test "tcp[tcpflags] = tcp-urg" "ether proto \ip && ip proto \tcp && tcp[13] = 0x20"
test "tcp[tcpflags] = tcp-ece" "ether proto \ip && ip proto \tcp && tcp[tcpflags] = tcp-ece"
test "tcp[tcpflags] = tcp-cwr" "ether proto \ip && ip proto \tcp && tcp[tcpflags] = tcp-cwr"

test "icmp[icmptype] = 0" "ether proto \ip && ip proto \icmp && icmp[0] = 0"
test "icmp[icmpcode] = 0" "ether proto \ip && ip proto \icmp && icmp[1] = 0"

test "icmp[icmptype] = icmp-echoreply" "ether proto \ip && ip proto \icmp && icmp[0] = 0"
test "icmp[icmptype] = icmp-unreach" "ether proto \ip && ip proto \icmp && icmp[0] = 3"
test "icmp[icmptype] = icmp-sourcequench" "ether proto \ip && ip proto \icmp && icmp[0] = 4"
test "icmp[icmptype] = icmp-redirect" "ether proto \ip && ip proto \icmp && icmp[0] = 5"
test "icmp[icmptype] = icmp-echo" "ether proto \ip && ip proto \icmp && icmp[0] = 8"
test "icmp[icmptype] = icmp-routeradvert" "ether proto \ip && ip proto \icmp && icmp[0] = 9"
test "icmp[icmptype] = icmp-routersolicit" "ether proto \ip && ip proto \icmp && icmp[0] = 10"
test "icmp[icmptype] = icmp-timxceed" "ether proto \ip && ip proto \icmp && icmp[0] = 11"
test "icmp[icmptype] = icmp-paramprob" "ether proto \ip && ip proto \icmp && icmp[0] = 12"
test "icmp[icmptype] = icmp-tstamp" "ether proto \ip && ip proto \icmp && icmp[0] = 13"
test "icmp[icmptype] = icmp-tstampreply" "ether proto \ip && ip proto \icmp && icmp[0] = 14"
test "icmp[icmptype] = icmp-ireq" "ether proto \ip && ip proto \icmp && icmp[0] = 15"
test "icmp[icmptype] = icmp-ireqreply" "ether proto \ip && ip proto \icmp && icmp[0] = 16"
test "icmp[icmptype] = icmp-maskreq" "ether proto \ip && ip proto \icmp && icmp[0] = 17"
test "icmp[icmptype] = icmp-maskreply" "ether proto \ip && ip proto \icmp && icmp[0] = 18"

test "icmp6[icmp6type] = 0" "ether proto \ip6 && ip6 proto \icmp6 && icmp6[icmp6type] = 0"
test "icmp6[icmp6code] = 0" "ether proto \ip6 && ip6 proto \icmp6 && icmp6[icmp6code] = 0"

test "icmp6[icmp6type] = icmp6-destinationunreach" "ether proto \ip6 && ip6 proto \icmp6 && icmp6[icmp6type] = 1"
test "icmp6[icmp6type] = icmp6-packettoobig" "ether proto \ip6 && ip6 proto \icmp6 && icmp6[icmp6type] = 2"
test "icmp6[icmp6type] = icmp6-timeexceeded" "ether proto \ip6 && ip6 proto \icmp6 && icmp6[icmp6type] = 3"
test "icmp6[icmp6type] = icmp6-parameterproblem" "ether proto \ip6 && ip6 proto \icmp6 && icmp6[icmp6type] = 4"
test "icmp6[icmp6type] = icmp6-echo" "ether proto \ip6 && ip6 proto \icmp6 && icmp6[icmp6type] = 128"
test "icmp6[icmp6type] = icmp6-echoreply" "ether proto \ip6 && ip6 proto \icmp6 && icmp6[icmp6type] = 129"
test "icmp6[icmp6type] = icmp6-multicastlistenerquery" "ether proto \ip6 && ip6 proto \icmp6 && icmp6[icmp6type] = 130"
test "icmp6[icmp6type] = icmp6-multicastlistenerreportv1" "ether proto \ip6 && ip6 proto \icmp6 && icmp6[icmp6type] = 131"
test "icmp6[icmp6type] = icmp6-multicastlistenerdone" "ether proto \ip6 && ip6 proto \icmp6 && icmp6[icmp6type] = 132"
test "icmp6[icmp6type] = icmp6-routersolicit" "ether proto \ip6 && ip6 proto \icmp6 && icmp6[icmp6type] = 133"
test "icmp6[icmp6type] = icmp6-routeradvert" "ether proto \ip6 && ip6 proto \icmp6 && icmp6[icmp6type] = 134"
test "icmp6[icmp6type] = icmp6-neighborsolicit" "ether proto \ip6 && ip6 proto \icmp6 && icmp6[icmp6type] = 135"
test "icmp6[icmp6type] = icmp6-neighboradvert" "ether proto \ip6 && ip6 proto \icmp6 && icmp6[icmp6type] = 136"
test "icmp6[icmp6type] = icmp6-redirect" "ether proto \ip6 && ip6 proto \icmp6 && icmp6[icmp6type] = 137"
test "icmp6[icmp6type] = icmp6-routerrenum" "ether proto \ip6 && ip6 proto \icmp6 && icmp6[icmp6type] = 138"
test "icmp6[icmp6type] = icmp6-nodeinformationquery" "ether proto \ip6 && ip6 proto \icmp6 && icmp6[icmp6type] = 139"
test "icmp6[icmp6type] = icmp6-nodeinformationresponse" "ether proto \ip6 && ip6 proto \icmp6 && icmp6[icmp6type] = 140"
test "icmp6[icmp6type] = icmp6-ineighbordiscoverysolicit" "ether proto \ip6 && ip6 proto \icmp6 && icmp6[icmp6type] = 141"
test "icmp6[icmp6type] = icmp6-ineighbordiscoveryadvert" "ether proto \ip6 && ip6 proto \icmp6 && icmp6[icmp6type] = 142"
test "icmp6[icmp6type] = icmp6-multicastlistenerreportv2" "ether proto \ip6 && ip6 proto \icmp6 && icmp6[icmp6type] = 143"
test "icmp6[icmp6type] = icmp6-homeagentdiscoveryrequest" "ether proto \ip6 && ip6 proto \icmp6 && icmp6[icmp6type] = 144"
test "icmp6[icmp6type] = icmp6-homeagentdiscoveryreply" "ether proto \ip6 && ip6 proto \icmp6 && icmp6[icmp6type] = 145"
test "icmp6[icmp6type] = icmp6-mobileprefixsolicit" "ether proto \ip6 && ip6 proto \icmp6 && icmp6[icmp6type] = 146"
test "icmp6[icmp6type] = icmp6-mobileprefixadvert" "ether proto \ip6 && ip6 proto \icmp6 && icmp6[icmp6type] = 147"
test "icmp6[icmp6type] = icmp6-certpathsolicit" "ether proto \ip6 && ip6 proto \icmp6 && icmp6[icmp6type] = 148"
test "icmp6[icmp6type] = icmp6-certpathadvert" "ether proto \ip6 && ip6 proto \icmp6 && icmp6[icmp6type] = 149"
test "icmp6[icmp6type] = icmp6-multicastrouteradvert" "ether proto \ip6 && ip6 proto \icmp6 && icmp6[icmp6type] = 151"
test "icmp6[icmp6type] = icmp6-multicastroutersolicit" "ether proto \ip6 && ip6 proto \icmp6 && icmp6[icmp6type] = 152"
test "icmp6[icmp6type] = icmp6-multicastrouterterm" "ether proto \ip6 && ip6 proto \icmp6 && icmp6[icmp6type] = 153"
