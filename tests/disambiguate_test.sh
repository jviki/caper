#!/bin/bash
#
# Test consists of checking that disambiguating an expression returns the
#   expected result.
# Nik Sultana, October 2022.
#
# NOTE error output from Caper is sent to /dev/null

CAPER="$1"
QUERY="$2"
EXPECTATION="$3"

if [[ "${CAPER}" = "" || "${QUERY}" = "" || "${EXPECTATION}" = "" ]]
then
  echo "Need to specify path to Caper, a query, and the expected result." >&2
  exit 1
fi

TEMP=TEMP_${RANDOM}
# FIXME check that ${TEMP} doesn't exist

RESULT=`echo "${QUERY}" | ${CAPER} 2> ${TEMP}`

ERROR_OUT=`cat ${TEMP}`
rm ${TEMP}
if [[ "${ERROR_OUT}" != "" ]]
then
  ERROR_OUT=". stderr output: '${ERROR_OUT}'"
fi

[[ "${EXPECTATION}" = "${RESULT}" ]] && echo "OK : ${QUERY}${ERROR_OUT}" || echo "FAIL (${QUERY}) : Expected '${EXPECTATION}' but got '${RESULT}'${ERROR_OUT}"
