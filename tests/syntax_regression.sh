#!/bin/bash
#
# Caper: a pcap expression analysis utility.
# Regression tests for parsing/printing
# Nik Sultana, August 2015

CAPER="$1"
TEST_SCRIPT="$2"

function test() {
  ${TEST_SCRIPT} "${CAPER}" "$1"
}

# These examples of pcap syntax are drawn from
#   https://www.wireshark.org/docs/man-pages/pcap-filter.html

test "host foo"
test "net 128.3"
test "port 20"
test "portrange 6000-6008"

test "src foo"
test "dst net 128.3"
test "src or dst port ftp-data"

test "ether src foo"
test "arp net 128.3"
test "tcp port 21"
test "udp portrange 7000-7009"
test "wlan addr2 0:2:3:4:5:6"
test "ether 0:2:3:4:5:6"
test "ether host 0:2:3:4:5:6"
test "ether src host 0:2:3:4:5:6"
test "src foo"
  # means same thing as
#test "ip src foo or arp src foo or rarp src foo"
test "ip src foo || arp src foo || rarp src foo"
test "net bar"
  # means same thing as
#test "ip net bar or arp net bar or rarp net bar"
test "ip net bar || arp net bar || rarp net bar"
test "port 53"
  # means same thing as
#test "tcp port 53 or udp port 53"
test "tcp port 53 || udp port 53"

#test "host foo and not port ftp and not port ftp-data"
test "host foo && ! port ftp && ! port ftp-data"
#test "tcp dst port ftp or ftp-data or domain"
test "tcp dst port ftp || ftp-data || domain"
  # means same thing as
#test "tcp dst port ftp or tcp dst port ftp-data or tcp dst port domain"
test "tcp dst port ftp || tcp dst port ftp-data || tcp dst port domain"

test "dst host \host"
test "src host \host"
test "host \host"
test "ip host \host"
#test "ether proto \ip and host \host"
test "ether proto \ip && host \host"
test "ether dst ehost"
test "ether src ehost"
test "ether host ehost"
test "gateway \host"
#test "ether host ehost and not host \host"
test "ether host ehost && ! host \host"
test "dst net \net"
test "dst net 192.168.1.0"
test "dst net 192.168.1"
test "dst net 172.16"
test "dst net 10"
test "dst net 192.168.1.0 mask 255.255.255.0"
test "dst net 192.168.1 mask 255.255.255.0"
test "dst net 172.16 mask 255.255.0.0"
test "dst net 10 mask 255.0.0.0"
test "dst net 192.168.1.0/24"
test "dst net 192.168.1/24"
test "dst net 172.16/16"
test "dst net 10/8"
test "dst port 513"
test "dst port domain"
test "src port \port"
test "port \port"
test "dst portrange port1-port2"
test "src portrange port1-port2"
test "portrange port1-port2"
test "tcp src port \port"
#test "less length" # FIXME "length" should be a number, not symbol
test "less 10"
#test "len <= length" # FIXME can "length" be a symbol, or only a number?
test "len <= 400"
#test "greater length" # FIXME "length" should be a number, not symbol
test "greater 20"
#test "len >= length" # FIXME can "length" be a symbol, or only a number?
test "len >= 491"
test "ip proto protocol"
test "ip6 proto protocol"
test "ip6 protochain protocol"
test "ip6 protochain 6"
test "ip protochain protocol"
test "ether broadcast"
test "ip broadcast"
test "ether multicast"
test "ether[0] & 1 != 0"
test "ip multicast"
test "ip6 multicast"
test "ether proto protocol"
test "fddi protocol arp"
test "tr protocol arp"
test "wlan protocol arp"
test "decnet src host"
test "ip[0] & 0xf != 5"
#test "ip[6:2] & 0x1fff = 0"
test "ip[6 : 2] & 0x1fff = 0" #NOTE the test-passed check is whitespace sensitive
#test "not host vs and ace"
test "! host vs && ace"
#test "not host vs and host ace"
test "! host vs && host ace"
#test "not ( host vs or ace )"
test "! (host vs || ace)"

test "host sundown"
#test "host helios and ( hot or ace )"
test "host helios && (hot || ace)"
#test "ip host ace and not helios"
test "ip host ace && ! helios"
test "net ucb-ether"
#test "gateway snup and (port ftp or ftp-data)"
test "gateway snup && (port ftp || ftp-data)"
#test "ip and not net localnet"
test "ip && ! net localnet"
#test "tcp[tcpflags] & (tcp-syn|tcp-fin) != 0 and not src and dst net localnet"
#test "tcp[tcpflags] & (tcp-syn|tcp-fin) != 0 && ! src && dst net localnet"
test "tcp[tcpflags] & (tcp-syn | tcp-fin) != 0 && ! src && dst net localnet" #NOTE the test-passed check is whitespace sensitive
#test "tcp port 80 and (((ip[2:2] - ((ip[0]&0xf)<<2)) - ((tcp[12]&0xf0)>>2)) != 0)"
#test "tcp port 80 && (((ip[2:2] - ((ip[0]&0xf)<<2)) - ((tcp[12]&0xf0)>>2)) != 0)"
test "tcp port 80 && (ip[2 : 2] - (ip[0] & 0xf) << 2) - (tcp[12] & 0xf0) >> 2 != 0" #NOTE the test-passed check is whitespace sensitive
#test "gateway snup and ip[2:2] > 576"
#test "gateway snup && ip[2:2] > 576"
test "gateway snup && ip[2 : 2] > 576" #NOTE the test-passed check is whitespace sensitive
#test "ether[0] & 1 = 0 and ip[16] >= 224"
test "ether[0] & 1 = 0 && ip[16] >= 224"
#test "icmp[icmptype] != icmp-echo and icmp[icmptype] != icmp-echoreply"
test "icmp[icmptype] != icmp-echo && icmp[icmptype] != icmp-echoreply"

test "vlan"
test "vlan 100"
test "vlan && vlan 100"
