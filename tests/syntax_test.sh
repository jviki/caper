#!/bin/bash
#
# Test consists of checking that parsing and printing a query returns the
#   original query.
# Nik Sultana, August 2015
#
# NOTE error output from Caper is sent to /dev/null

CAPER="$1"
QUERY="$2"

if [[ "${CAPER}" = "" || "${QUERY}" = "" ]]
then
  echo "Need to specify path to Caper, and a query" >&2
  exit 1
fi

RESULT=`echo "${QUERY}" | ${CAPER} 2> /dev/null`

[[ "${QUERY}" = "${RESULT}" ]] && echo "OK : ${QUERY}" || echo "FAIL : '${QUERY}' vs '${RESULT}'"
