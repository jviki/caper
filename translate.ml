(*
  Copyright Nik Sultana, November 2018

  This file is part of Caper.

  Caper is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Caper is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Caper.  If not, see <https://www.gnu.org/licenses/>.


  This module: Translating pcap expressions into bit-vector logic
*)

open Pcap_syntax

let bind_opt_conj (formula_opt : Bitvector.bv_formula option) (formula : Bitvector.bv_formula) : Bitvector.bv_formula =
  match formula_opt with
  | None -> formula
  | Some formula' -> Bitvector.And [formula; formula']

let bind_opt_conj_opt (formula_opt1 : Bitvector.bv_formula option) (formula_opt2 : Bitvector.bv_formula option) : Bitvector.bv_formula option =
  match formula_opt1, formula_opt2 with
  | None, None -> None
  | None, Some _ -> formula_opt2
  | Some _, None -> formula_opt1
  | Some formula1, Some formula2 -> Some (Bitvector.And [formula1; formula2])

let rec translate_e
 ?emit_explanation:(emit_explanation = !Config.diagnostic_output_mode)
 (e : expr) (size_opt : Bitvector.width option) : Bitvector.bv_exp * Bitvector.bv_formula option =
  match e with
  | Identifier s ->
      begin
        let mapped_value : int =
          match s with
          | _ -> failwith (Aux.error_output_prefix ^ ": " ^ "TODO: translate_e Identifier") in
        (Bitvector.Const (size_opt, mapped_value), None)
      end
  | Nat_Literal n ->
      (*FIXME if size_opt <> None, then check that "n" can be encoded in that size*)
      (Bitvector.Const (size_opt, n), None)
  | Hex_Literal h ->
      let n = int_of_string h in
      (*FIXME if size_opt <> None, then check that "n" can be encoded in that size*)
      (Bitvector.Const (size_opt, n), None)
  | Packet_Access (prot_s, e', nb_opt) ->
      (*Map e' to numeric offset, and based on nb_opt use "packet_proj_X"*)
      let nb, projection =
        match nb_opt with
        | None -> (Bitvector.bpf_size_byte, "packet_proj_8 ")
        | Some One -> (Bitvector.bpf_size_byte, "packet_proj_8 ")
        | Some Two ->  (Bitvector.bpf_size_halfword, "packet_proj_16 ")
        | Some Four -> (Bitvector.bpf_size_word, "packet_proj_32 ") in
      let translated_e', constraint_formula =
        let ip_offset e =
          Bitvector.BPlus (Some Bitvector.bpf_size_word, e,
             Bitvector.Const (Some Bitvector.bpf_size_word, 14 + !Prim_translate.packet_offset)) in
        let basic, constraint_formula' = translate_e e' (Some Bitvector.bpf_size_word) in
        match prot_s with
        | "ip" ->
          (ip_offset basic,
             bind_opt_conj_opt constraint_formula' (Some Prim_translate.ipv4_is_zero_fragment))
        | "tcp" ->
          (Prim_translate.packet_offset_tcp basic
           |> ip_offset,
             bind_opt_conj_opt constraint_formula' (Some Prim_translate.ipv4_is_zero_fragment)) in

      let extend_by =
        match size_opt with
        | None -> 0
        | Some expected ->
            if nb = expected then 0
            else if nb < expected then expected - nb
            else failwith (Aux.error_output_prefix ^ ": " ^ "Translation: width: expected=" ^
              string_of_int expected ^
              " actual=" ^ string_of_int nb ^
              " expression=" ^ Pcap_syntax_aux.string_of_expr e) in

      let actual_size_opt = Some nb in

      let pre_result =
        Bitvector.Hole (actual_size_opt, "(" ^ projection ^
        Bitvector.string_of_bv_exp translated_e' ^
        ")") in
      let result =
        if extend_by = 0 then pre_result
        else
         begin
           if emit_explanation then
            (*FIXME use Aux.error_output*)
            Printf.eprintf "Extending width by %d for %s\n" extend_by (Bitvector.string_of_bv_exp pre_result);
           Bitvector_sizing.extend_bv extend_by pre_result
         end in
      (result, constraint_formula)
  | Len ->
      (*NOTE translate to a name in the target theory*)
      let actual_size_opt = Some Bitvector.bpf_size_word in
      assert (size_opt = None || size_opt = actual_size_opt);
      (Bitvector.Hole (actual_size_opt, "len"), None)
  | Plus es ->
      begin
        match es with
        | [] -> failwith (Aux.error_output_prefix ^ ": " ^ "Empty Plus")
        | [e'] -> translate_e e' size_opt
        | e' :: es' ->
          begin
          let bv_e', constraint1 = translate_e e' size_opt in
          let bv_es, constraint2 = translate_e (Plus es') size_opt in
          (Bitvector.BPlus(size_opt, bv_e', bv_es),
           bind_opt_conj_opt constraint1 constraint2)
          end
      end
  | Times es ->
      begin
        match es with
        | [] -> failwith (Aux.error_output_prefix ^ ": " ^ "Empty Times")
        | [e'] -> translate_e e' size_opt
        | e' :: es' ->
          begin
          let bv_e', constraint1 = translate_e e' size_opt in
          let bv_es, constraint2 = translate_e (Times es') size_opt in
          (Bitvector.BMul(size_opt, bv_e', bv_es),
           bind_opt_conj_opt constraint1 constraint2)
          end
      end
  | Binary_And es ->
      begin
        match es with
        | [] -> failwith (Aux.error_output_prefix ^ ": " ^ "Empty Binary_And")
        | [e'] -> translate_e e' size_opt
        | e' :: es' ->
          begin
          let bv_e', constraint1 = translate_e e' size_opt in
          let bv_es, constraint2 = translate_e (Binary_And es') size_opt in
          (Bitvector.BAnd(size_opt, bv_e', bv_es),
           bind_opt_conj_opt constraint1 constraint2)
          end
      end
  | Binary_Or es ->
      begin
        match es with
        | [] -> failwith (Aux.error_output_prefix ^ ": " ^ "Empty Binary_Or")
        | [e'] -> translate_e e' size_opt
        | e' :: es' ->
          begin
          let bv_e', constraint1 = translate_e e' size_opt in
          let bv_es, constraint2 = translate_e (Binary_Or es') size_opt in
          (Bitvector.BOr(size_opt, bv_e', bv_es),
           bind_opt_conj_opt constraint1 constraint2)
          end
      end
  | Binary_Xor es ->
      begin
        match es with
        | [] -> failwith (Aux.error_output_prefix ^ ": " ^ "Empty Binary_Xor")
        | [e'] -> translate_e e' size_opt
        | e' :: es' ->
          begin
          let bv_e', constraint1 = translate_e e' size_opt in
          let bv_es, constraint2 = translate_e (Binary_Xor es') size_opt in
          (Bitvector.BXor(size_opt, bv_e', bv_es),
           bind_opt_conj_opt constraint1 constraint2)
          end
      end

  | Minus (e1, e2) ->
      begin
      let bv_e1, constraint1 = translate_e e1 size_opt in
      let bv_e2, constraint2 = translate_e e2 size_opt in
      (Bitvector.BMul(size_opt, bv_e1, bv_e2),
       bind_opt_conj_opt constraint1 constraint2)
      end
  | Quotient (e1, e2) ->
      begin
      let bv_e1, constraint1 = translate_e e1 size_opt in
      let bv_e2, constraint2 = translate_e e2 size_opt in
      (Bitvector.BDiv(size_opt, bv_e1, bv_e2),
       bind_opt_conj_opt constraint1 constraint2)
      end
  | Mod (e1, e2) ->
      begin
      let bv_e1, constraint1 = translate_e e1 size_opt in
      let bv_e2, constraint2 = translate_e e2 size_opt in
      (Bitvector.BMod(size_opt, bv_e1, bv_e2),
       bind_opt_conj_opt constraint1 constraint2)
      end
  | Shift_Right (e1, e2) ->
      begin
      let bv_e1, constraint1 = translate_e e1 size_opt in
      let bv_e2, constraint2 = translate_e e2 size_opt in
      (Bitvector.LShRight(size_opt, bv_e1, bv_e2),
       bind_opt_conj_opt constraint1 constraint2)
      end
  | Shift_Left (e1, e2) ->
      begin
      let bv_e1, constraint1 = translate_e e1 size_opt in
      let bv_e2, constraint2 = translate_e e2 size_opt in
      (Bitvector.ShLeft(size_opt, bv_e1, bv_e2),
       bind_opt_conj_opt constraint1 constraint2)
      end

let rec translate_pe (pe : pcap_expression) : Bitvector.bv_formula =
  match pe with
  | Primitive prim -> Prim_translate.translate_to_smt prim
  | Atom re ->
      begin
        match re with
        | Gt (e1, e2) ->
            begin
            let bv_e1, constraint1 = translate_e e1 None in
            let bv_e2, constraint2 = translate_e e2 None in
            Bitvector.Atom (Bitvector.UGt(bv_e1, bv_e2))
            |> bind_opt_conj constraint1
            |> bind_opt_conj constraint2
            end
        | Lt (e1, e2) ->
            begin
            let bv_e1, constraint1 = translate_e e1 None in
            let bv_e2, constraint2 = translate_e e2 None in
            Bitvector.Atom (Bitvector.ULt(bv_e1, bv_e2))
            |> bind_opt_conj constraint1
            |> bind_opt_conj constraint2
            end
        | Geq (e1, e2) ->
            begin
            let bv_e1, constraint1 = translate_e e1 None in
            let bv_e2, constraint2 = translate_e e2 None in
            Bitvector.Atom (Bitvector.UGeq(bv_e1, bv_e2))
            |> bind_opt_conj constraint1
            |> bind_opt_conj constraint2
            end
        | Leq (e1, e2) ->
            begin
            let bv_e1, constraint1 = translate_e e1 None in
            let bv_e2, constraint2 = translate_e e2 None in
            Bitvector.Atom (Bitvector.ULeq(bv_e1, bv_e2))
            |> bind_opt_conj constraint1
            |> bind_opt_conj constraint2
            end
        | Eq (e1, e2) ->
            begin
            let bv_e1, constraint1 = translate_e e1 None in
            let bv_e2, constraint2 = translate_e e2 None in
            Bitvector.Atom (Bitvector.Eq(bv_e1, bv_e2))
            |> bind_opt_conj constraint1
            |> bind_opt_conj constraint2
            end
        | Neq (e1, e2) ->
            begin
            let bv_e1, constraint1 = translate_e e1 None in
            let bv_e2, constraint2 = translate_e e2 None in
            Bitvector.Not (Bitvector.Atom (Bitvector.Eq(bv_e1, bv_e2)))
            |> bind_opt_conj constraint1
            |> bind_opt_conj constraint2
            end
      end
  | And pes -> Bitvector.And (List.map translate_pe pes)
  | Or pes -> Bitvector.Or (List.map translate_pe pes)
  | Not pe' -> Bitvector.Not (translate_pe pe')
  | True -> Bitvector.True
  | False -> Bitvector.True
